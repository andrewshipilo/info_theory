#ifndef INFO_THEORY_PROBABILITY_H
#define INFO_THEORY_PROBABILITY_H

#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include <algorithm>

class Probability {
public:
    class Item {
    public:
        Item() : character("a"), probability(0) {}

        Item(std::string c, float p) : character(std::move(c)), probability(p) {}

        bool operator<(const Item &item) const {
            return (probability < item.probability);
        }

        std::string character;
        float probability;

    };

    Probability() = default;

    Probability(const Probability &other);

    explicit Probability(const std::string &str);

    void
    generateProbability(Probability &currentProbability, Probability &defaultProbability, int currDeep,
                        int maxDeep);

    void print();

    void sort();

    void generateEntropy();

    unsigned long size();

    float getEntropy() const;

    double findErrorProbability(double codeLength);

    Probability &resizeProbability(Probability &defaultProbability);

    Probability &operator=(const Probability &other);

private:
    std::vector<Item> m_charProbability;
    float H = 0;
};

#endif
