//
// Created by andrew on 27.10.17.
//

#include <cmath>
#include <iomanip>
#include <fstream>
#include "Probability.h"

Probability::Probability(const std::string &str) {
    std::vector<Item> result(256);
    for (int i = 0; i < result.size(); i++) {
        result[i].character = static_cast<char>(i);
    }

    for (char c : str) {
        result[c].probability += 1;
    }

    for (auto c : result) {
        if (c.probability != 0) {
            m_charProbability.emplace_back(c.character, c.probability / str.length());
        }
    }
}

Probability::Probability(const Probability &other) {
    for (const auto &item : other.m_charProbability) {
        m_charProbability.push_back(item);
    }
    this->H = other.getEntropy();
}

Probability &Probability::operator=(const Probability &other) {
    for (const auto &item : other.m_charProbability) {
        m_charProbability.push_back(item);
    }
    this->H = other.getEntropy();
    return *this;
}

void Probability::print() {
    std::cout << "Probability of  " << m_charProbability.size() << " elements" << std::endl;
    for (auto c : m_charProbability) {
        std::cout << c.character << " : " << std::setprecision(5) << c.probability << " ";
    }
    std::cout << std::endl;
}

void Probability::generateEntropy() {
    for (auto item : m_charProbability) {
        H -= item.probability * (std::log10(item.probability) / std::log10(2));
    }
}

void
Probability::generateProbability(Probability &currentProbability, Probability &defaultProbability, int currDeep,
                                 int maxDeep) {
    if (currDeep >= maxDeep)
        return;

    currentProbability.generateEntropy();
    double entropy = currentProbability.getEntropy();
    double N = std::ceil(log2(currentProbability.size()));
    int n = currDeep + 1;
    double R = N / (n);
    entropy /= n;

    if (currDeep == 0) {
        std::cout << std::setw(15) << "Length"
               << std::setw(15) << "N"
               << std::setw(15) << "R(ate)"
               << std::setw(15) << "Entropy"
               << std::setw(15) << "Error"
               << std::endl;
    }

    for (; R >= entropy || (N + 1) / n >= entropy;) {
        R = N / (n);
        double errorProb = currentProbability.findErrorProbability(N);
        std::cout << std::setw(15) << n
               << std::setw(15) << N
               << std::setw(15) << R
               << std::setw(15) << entropy
               << std::setw(15) << errorProb
               << std::endl;
        N--;
    }
    std::cout << std::endl;

    Probability newProbability = currentProbability.resizeProbability(defaultProbability);
    generateProbability(newProbability, defaultProbability, currDeep + 1, maxDeep);

}

void Probability::sort() {
    std::sort(m_charProbability.begin(), m_charProbability.end());
}

unsigned long Probability::size() {
    return m_charProbability.size();
}

float Probability::getEntropy() const {
    return H;
}


double Probability::findErrorProbability(double codeLength) {
    unsigned long alphabetSize = this->size();

    double currentCharCount = pow(2, codeLength);

    double character = alphabetSize - currentCharCount;

    this->sort();

    double Pe = 0;

    for (int i = 0; i < character; ++i) {
        Pe += m_charProbability[i].probability;
    }

    return Pe;
}

Probability &Probability::resizeProbability(Probability &defaultProbability) {
    auto *newProbability = new Probability();

    for (auto &i : m_charProbability) {

        for (auto &j : defaultProbability.m_charProbability) {

            std::string charSequence = i.character + j.character;
            double probability = i.probability * j.probability;
            newProbability->m_charProbability.emplace_back(charSequence, probability);
        }
    }
    return *newProbability;
}
