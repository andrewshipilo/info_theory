#include <string>
#include <cstring>
#include <vector>
#include <iostream>

void moveToFront(int curr_index, std::string &list)
{
    const std::string record(list);

    // Characters pushed one position right in the list up until curr_index
    std::strncpy(const_cast<char *>(list.c_str() + 1), record.c_str(), curr_index);

    list[0] = record[curr_index];
}

void mtfEncode(const std::string &input_text, std::string &list)
{
    int i;
    std::cout << "MoveToFront Transform: ";
    std::vector<uint> output_arr(input_text.length());
    for (i = 0; i < input_text.length(); i++) {

        output_arr[i] = static_cast<uint>(list.find_first_of(input_text[i]));
        std::cout << output_arr[i] << " ";
        moveToFront(output_arr[i], list);
    }
}

int main()
{
    std::string input_text = "andrew shipilo";
    std::string list = "abcdefghijklmnopqrstuvwxyz ,.!?:;\"";

    std::cout << input_text << std::endl;

    mtfEncode(input_text, list);
    return 0;
}

