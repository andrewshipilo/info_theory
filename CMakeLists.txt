cmake_minimum_required(VERSION 3.8)
project(info_theory)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp Probability.cpp Probability.h)
add_executable(info_theory ${SOURCE_FILES})